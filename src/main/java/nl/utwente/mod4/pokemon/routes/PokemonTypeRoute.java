package nl.utwente.mod4.pokemon.routes;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nl.utwente.mod4.pokemon.dao.PokemonTypeDao;
import nl.utwente.mod4.pokemon.model.PokemonType;
import nl.utwente.mod4.pokemon.model.ResourceCollection;

import java.util.List;

@Path("/pokemonTypes")
public class PokemonTypeRoute {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResourceCollection<PokemonType> getPokemonTypes(
            @QueryParam("sortBy") String sortBy,
            @QueryParam("pageSize") int pageSize,
            @QueryParam("pageNumber") int pageNumber
    ) {
        int ps = pageSize > 0 ? pageSize : Integer.MAX_VALUE;
        int pn = pageNumber > 0 ? pageNumber : 1;
        var resources = PokemonTypeDao.INSTANCE.getPokemonTypes(ps, pn, sortBy).toArray(new PokemonType[0]);
        var total = PokemonTypeDao.INSTANCE.getTotalPokemonTypes();

        return new ResourceCollection<>(resources, ps, pn, total);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public PokemonType createPokemonType(PokemonType pokemonType) {
        return PokemonTypeDao.INSTANCE.create(pokemonType);
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public PokemonType getPokemonType(@PathParam("id") String id) {
        return PokemonTypeDao.INSTANCE.getPokemonType(id);
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public PokemonType updatePokemonType(@PathParam("id") String id, PokemonType toUpdate) {
        if (id == null || !id.equals(toUpdate.id))
            throw new BadRequestException("Id mismatch.");

        return PokemonTypeDao.INSTANCE.update(toUpdate);
    }

    @DELETE
    @Path("/{id}")
    public void deletePokemonType(@PathParam("id") String id) {
        PokemonTypeDao.INSTANCE.delete(id);
    }
}